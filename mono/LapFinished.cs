using System;
using Newtonsoft.Json.Linq;

static class LapFinished
{
    /// <summary>
    /// Process the specified msgData and raceData for the LapFinished msgType.
    /// </summary>
    /// <param name="msgData">Message data</param>
    /// <param name="raceData">Race data</param>
    internal static SendMsg process(string msgData, RaceData raceData)
    {
        JObject parsedJson = JObject.Parse(msgData);
        string name = parsedJson["car"]["name"].ToObject<string>();
        string color = parsedJson["car"]["color"].ToObject<string>();
        if (name.Equals(raceData.BotName) && color.Equals(raceData.BotColor))
        {
            int lapFinished = parsedJson["lapTime"]["lap"].ToObject<int>();
            int time = parsedJson["lapTime"]["millis"].ToObject<int>();
            Console.WriteLine(string.Format("Lap {0} finished. Time: {1}", lapFinished, time));
            raceData.CurrentLap = lapFinished + 1;
        }

        return Ping.One;
    }
}
