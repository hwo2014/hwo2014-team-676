using System;

class Join: SendMsg {
    public string name;
    public string key;

    public Join(string name, string key) {
        this.name = name;
        this.key = key;
    }

    protected override string MsgType() { 
        return "join";
    }
}
