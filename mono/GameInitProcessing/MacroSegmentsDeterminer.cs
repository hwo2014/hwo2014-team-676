using System;

/// <summary>
/// Determines the macro segments of track pieces, flagging them as dangerous curve or straight line zones.
/// </summary>
class MacroSegmentsDeterminer : IGameInitProcessor
{
    private RaceData raceData;

    TrackPiece currentTrackPiece;
    TrackPiece nextPiece;
    TrackSegment currentSegment = new TrackSegment(0);

    public MacroSegmentsDeterminer(RaceData raceData)
    {
        this.raceData = raceData;

        currentTrackPiece = raceData.TrackPieces[0];
        currentSegment.IsDangerous = currentTrackPiece.IsDangerous;
        raceData.TrackSegments.Add(currentSegment);
    }

    public void ProcessCurrentPiece(int trackPieceIndex)
    {
        this.currentTrackPiece = raceData.TrackPieces[trackPieceIndex];

        if (currentTrackPiece.IsDangerous != currentSegment.IsDangerous) {
            // the "dangerness" changed from the current track piece to the current segment.
            // we might want to close the current segment and open a new one.
            // to decide that, as we are more tolerant when creating dangerous segments (a straight line in
            // the middle of curves is still part of a dangerous segment), if we are going from dangerous piece
            // to safe, we cannot decide it yet
            if (currentTrackPiece.IsDangerous) {
                // moving from safe -> dangerous: peeks ahead. If there are no "too dangerous" pieces, stick on
                // the safe segment
                nextPiece = raceData.TrackPieces[(trackPieceIndex + 1) % raceData.TrackPieces.Count];
                if (nextPiece.IsDangerous) {
                    currentSegment.EndingTrackPieceIndex = trackPieceIndex - 1;
                    currentSegment = new TrackSegment(trackPieceIndex);
                    currentSegment.IsDangerous = true;
                    raceData.TrackSegments.Add(currentSegment);
                }

            } else if (!currentTrackPiece.IsDangerous) {
                // moving from dangerous -> safe: peek the next 4-5 pieces to decide if we are indeed starting
                // a safe segment or if we keep building the dangerous
                // if there are 2 dangerous pieces, we stick in the dangerous segment
                int dangerousPiecesAhead = 0;
                for (int j = 1; j <= 5 && dangerousPiecesAhead < 2; j++) {
                    nextPiece = raceData.TrackPieces[(trackPieceIndex + j) % raceData.TrackPieces.Count];
                    if (nextPiece.IsDangerous) {
                        dangerousPiecesAhead++;
                    }
                }

                if (dangerousPiecesAhead < 2) {
                    // it is ok to close the current dangerous segment now and open a safe one
                    currentSegment.EndingTrackPieceIndex = trackPieceIndex - 1;
                    currentSegment = new TrackSegment(trackPieceIndex);
                    currentSegment.IsDangerous = false;
                    raceData.TrackSegments.Add(currentSegment);
                }
            }

        } else {
            // TODO: always using the first lane. but his might be a reasonable approximation
            currentSegment.Length += currentTrackPiece.Length[0];
        }
    }

    public void AfterLoopProcess()
    {
        // the last segment might not be closed.
        // check if the last segment matches the first
        TrackSegment firstTrackSegment = raceData.TrackSegments[0];
        TrackSegment lastTrackSegment = currentSegment;
        if (lastTrackSegment == firstTrackSegment) {
            // there is only a single segment. close it.
            firstTrackSegment.EndingTrackPieceIndex = raceData.TrackPieces.Count - 1;
        } else if (lastTrackSegment.IsDangerous == firstTrackSegment.IsDangerous) {
            // there is more than 1 segment. the first and last have the same "dangerness".
            // hence, we can merge them together
            firstTrackSegment.StartingTrackPieceIndex = lastTrackSegment.StartingTrackPieceIndex;
            firstTrackSegment.Merged = true;
            raceData.TrackSegments.Remove(lastTrackSegment);
        } else {
            // there is more than 1 segment. the first and last diverge in their "dangerness".
            // simply close the last one
            lastTrackSegment.EndingTrackPieceIndex = raceData.TrackPieces.Count - 1;
        }

        Console.WriteLine("Track segments: ");
        raceData.TrackSegments.ForEach(ts => Console.WriteLine(string.Format("From: {0}, To: {1} - Dangerous: {2} - L({3}) - Merged: {4}", ts.StartingTrackPieceIndex, ts.EndingTrackPieceIndex, ts.IsDangerous, ts.Length, ts.Merged)));
    }
}
