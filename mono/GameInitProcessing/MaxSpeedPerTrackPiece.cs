using System;
using System.Collections.Generic;

/// <summary>
/// Finds the max speed per track piece.
/// </summary>
class MaxSpeedPerTrackPiece : IGameInitProcessor
{
    private RaceData raceData;

    TrackPiece currentTrackPiece;
    int numberOfLanes;
    double maxSpeedOnLane = 0.0;

    public MaxSpeedPerTrackPiece(RaceData raceData)
    {
        this.raceData = raceData;

        numberOfLanes = raceData.LanesDistanceFromCenter.Count;
    }

    public void ProcessCurrentPiece(int trackPieceIndex)
    {
        this.currentTrackPiece = raceData.TrackPieces[trackPieceIndex];

        Console.Write("Max speed on piece ");
        Console.Write(trackPieceIndex);
        Console.Write(":|\t");

        raceData.MaxSpeedPerPiecePerLane.Add(new List<double>());
        for (int j = 0; j < numberOfLanes; j++)
        {
            if (currentTrackPiece.Angle == 0)
            {
                maxSpeedOnLane = Parameters.MAX_SPEED;
            }
            else
            {
                maxSpeedOnLane = Math.Min(Parameters.MAX_INITIAL_SPEED_CURVE,
                    (Parameters.MAX_ANGULAR_SPEED * currentTrackPiece.Length[j]) /
                    (Math.Abs(currentTrackPiece.Angle) / 360 ));
            }
            raceData.MaxSpeedPerPiecePerLane[trackPieceIndex].Add(maxSpeedOnLane);
            Console.Write(maxSpeedOnLane);
            Console.Write("|\t");
        }
        Console.WriteLine();
    }

    public void AfterLoopProcess() { }
}
