using System;
using System.Collections.Generic;

/// <summary>
/// Classifies the macro segments and/or their track pieces.
/// It marks who is:
///  - the last safe segment/first track piece inside it before the lap finishes
///  - the track piece in each safe segment in which the deceleration should start
///    (as we're entering a dangerous segment)
///  - the first track piece in each safe segment to begin full acceleration
///  - TODO: good pieces/segments to overtake
///  - TODO: good pieces/segments to bump
/// </summary>
class MacroSegmentsAndPiecesClassifier : IGameInitProcessor
{
    private RaceData raceData;

    public MacroSegmentsAndPiecesClassifier(RaceData raceData)
    {
        this.raceData = raceData;
    }

    public void ProcessCurrentPiece(int trackPieceIndex) { }

    public void AfterLoopProcess()
    {
        TrackPiece piece;

        TrackSegment segment = raceData.TrackSegments.Find(ts => ts.Merged && !ts.IsDangerous);
        if (segment == null)
        {
            segment = raceData.TrackSegments.FindLast(ts => !ts.IsDangerous);
        }
        if (segment.Merged || segment == raceData.TrackSegments[raceData.TrackSegments.Count - 1]) {
            // the last segment is a safe one
            // we can mark it's first piece as a good place to start a sprint
            piece = raceData.TrackPieces[segment.StartingTrackPieceIndex];
            piece.Tags.Add(TrackPieceTag.LAST_SPRINT_BEFORE_LAP_FINISHES);
        }

        foreach (TrackSegment s in raceData.TrackSegments) {
            // sets the the piece in a segment that is around it's 70% as must for deceleration 
            int pieceIndexForDeceleration = s.EndingTrackPieceIndex - ((int)Math.Ceiling(s.LengthInPieces * 0.30));
            piece = raceData.TrackPieces[pieceIndexForDeceleration];
            piece.Tags.Add(TrackPieceTag.BEWARE_OF_THE_DANGEROUS_ZONE_AHEAD);
            // COMMENTED OUT: the car was just stopping because of MIN_THROTTLE on the curves
            //          for (int i = pieceIndexForDeceleration; i <= s.EndingTrackPieceIndex; i++){
            //              List<double> maxSpeedOnLane = raceData.MaxSpeedPerPiecePerLane[i];
            //              for (int j = 0; j < maxSpeedOnLane.Count; j++)
            //              {
            //                  raceData.MaxSpeedPerPiecePerLane[i][j] = Parameters.MIN_THROTTLE;
            //              }
            //          }

            // sets the first piece of every safe segment as a good place to begin full acceleration
            piece = raceData.TrackPieces[s.StartingTrackPieceIndex];
            piece.Tags.Add(TrackPieceTag.LONG_SAFE_LINE_AHEAD);
        }
    }
}
