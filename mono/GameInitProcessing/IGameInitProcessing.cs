using System;

interface IGameInitProcessor
{
    void ProcessCurrentPiece(int trackPieceIndex);
    void AfterLoopProcess();
}
