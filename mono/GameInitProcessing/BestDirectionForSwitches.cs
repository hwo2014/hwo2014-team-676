using System;
using System.Collections.Generic;

/// <summary>
/// Finds the best lanes to switch on each switch.
/// </summary>
class BestDirectionForSwitches : IGameInitProcessor
{
    private RaceData raceData;

    int lastSwitchIndex = -1;
    double accumulatedAngle = 0.0;
    TrackPiece currentTrackPiece;
    Dictionary<int, double> accumulatedAngles = new Dictionary<int, double>();
    Dictionary<int, int> nextSwitchIndex = new Dictionary<int, int>();

    public BestDirectionForSwitches(RaceData raceData)
    {
        this.raceData = raceData;
    }

    public void ProcessCurrentPiece(int trackPieceIndex)
    {
        this.currentTrackPiece = raceData.TrackPieces[trackPieceIndex];

        if (currentTrackPiece.Switch)
        {
            if (accumulatedAngle != 0)
            {
                accumulatedAngles.Add(lastSwitchIndex, accumulatedAngle);
                nextSwitchIndex.Add(lastSwitchIndex, trackPieceIndex);
            }
            lastSwitchIndex = trackPieceIndex;
            accumulatedAngle = 0.0;
        }
        else
        {
            accumulatedAngle += currentTrackPiece.Angle;
        }
    }

    public void AfterLoopProcess()
    {
        if (accumulatedAngles.ContainsKey(-1))
        {
            raceData.FirstSwitchDirection = accumulatedAngle > 0 ? "Right" : "Left";

            accumulatedAngle += accumulatedAngles[-1];
            if (accumulatedAngle != 0)
            {
                accumulatedAngles.Add(lastSwitchIndex, accumulatedAngle);
                nextSwitchIndex.Add(lastSwitchIndex, nextSwitchIndex[-1]);
            }
        }

        foreach (KeyValuePair<int, double> afterSwitchAngle in accumulatedAngles)
        {
            raceData.BestDirectionNextSwitch.Add(afterSwitchAngle.Key, afterSwitchAngle.Value > 0 ? "Right" : "Left");
            raceData.BestDirectionNextSwitchIndex.Add(afterSwitchAngle.Key, nextSwitchIndex[afterSwitchAngle.Key]);
        }
    }
}
