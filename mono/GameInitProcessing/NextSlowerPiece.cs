using System;
using System.Collections.Generic;

/// <summary>
/// Finds information regarding the next slower piece
/// IMPORTANT: This process need to be done after NextCurves.
/// </summary>
class NextSlowerPiece : IGameInitProcessor
{
    private RaceData raceData;

    Dictionary<int, int> nextSlowerPiece = new Dictionary<int, int>();
    Dictionary<int, List<double>> nextSlowerPieceDistance = new Dictionary<int, List<double>>();

    public NextSlowerPiece(RaceData raceData)
    {
        this.raceData = raceData;
    }

    public void ProcessCurrentPiece(int trackPieceIndex) { }

    public void AfterLoopProcess()
    {
        //obtains values already processed from next curves info
        for (int currentPiece = 0; currentPiece < raceData.NextCurvePiece.Count; currentPiece++)
        {
            int? firstSimilarPiece = null;
            List<double> firstSimilarPieceDistances = new List<double>();

            // curve values are not processed. Find next piece with lower radius
            if (raceData.NextCurvePiece[currentPiece] == currentPiece)
            {
                double currentRadius = Math.Min(
                    raceData.TrackPieces[currentPiece].Radius[0],
                    raceData.TrackPieces[currentPiece].Radius[raceData.LanesDistanceFromCenter.Count - 1]);
                List<double> distances = new List<double>();
                for (int lane = 0; lane < raceData.LanesDistanceFromCenter.Count; lane++)
                {
                    distances.Add(0);
                }
                for (int nextPiece = currentPiece + 1; nextPiece != currentPiece; nextPiece++)
                {
                    nextPiece %= raceData.NextCurvePiece.Count;
                    for (int lane = 0; lane < raceData.LanesDistanceFromCenter.Count; lane++)
                    {
                        distances[lane] += raceData.TrackPieces[nextPiece].Length[lane];
                    }

                    if (raceData.TrackPieces[nextPiece].Angle == 0)
                    {
                        continue;
                    }

                    double nextPieceRadius = Math.Min(
                        raceData.TrackPieces[nextPiece].Radius[0],
                        raceData.TrackPieces[nextPiece].Radius[raceData.LanesDistanceFromCenter.Count - 1]);
                    if (currentRadius >= nextPieceRadius)
                    {
                        nextSlowerPiece.Add(currentPiece, nextPiece);
                        nextSlowerPieceDistance.Add(currentPiece, distances);
                        break;
                    }
                    else if (!firstSimilarPiece.HasValue && currentRadius == nextPieceRadius)
                    {
                        firstSimilarPiece = nextPiece;
                        firstSimilarPieceDistances.AddRange(distances);
                    }
                }
                //this is the worse piece of the track - point it to the next similar piece
                if (!nextSlowerPiece.ContainsKey(currentPiece))
                {
                    nextSlowerPiece.Add(currentPiece, firstSimilarPiece.HasValue ? firstSimilarPiece.Value : currentPiece);
                    nextSlowerPieceDistance.Add(currentPiece, firstSimilarPiece.HasValue ? firstSimilarPieceDistances : distances);
                }
            }
            else
            {
                // values are already processed for straight lines
                nextSlowerPiece.Add(currentPiece, raceData.NextCurvePiece[currentPiece]);
                nextSlowerPieceDistance.Add(currentPiece, new List<double>());
                for (int lane = 0; lane < raceData.LanesDistanceFromCenter.Count; lane++)
                {
                    nextSlowerPieceDistance[currentPiece].Add(raceData.NextCurveDistance[currentPiece]);
                }
            }
        }

        Console.WriteLine("Next slower pieces:");
        for (int i = 0; i < raceData.TrackPieces.Count; i++)
        {
            raceData.NextSlowerPiece.Add(nextSlowerPiece[i]);
            raceData.NextSlowerPieceDistance.Add(nextSlowerPieceDistance[i]);

            Console.Write(string.Format("Piece {0}|\tNext: {1}|\tDistances: ", i, nextSlowerPiece[i]));
            for (int lane = 0; lane < raceData.LanesDistanceFromCenter.Count; lane++)
            {
                Console.Write(string.Format("{0}|\t", nextSlowerPieceDistance[i][lane]));
            }
            Console.WriteLine();
        }
    }
}
