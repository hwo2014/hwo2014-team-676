using System;
using System.Collections.Generic;

/// <summary>
/// Finds information regarding the next curve so the bot can choose better the throttle values.
/// </summary>
class NextCurves : IGameInitProcessor
{
    private RaceData raceData;

    TrackPiece currentTrackPiece;
    Dictionary<int, int> nextCurvePiece = new Dictionary<int, int>();
    Dictionary<int, double> nextCurveDistance = new Dictionary<int, double>();
    int lastStraightLineStart = 0;

    public NextCurves(RaceData raceData)
    {
        this.raceData = raceData;
    }

    public void ProcessCurrentPiece(int trackPieceIndex)
    {
        this.currentTrackPiece = raceData.TrackPieces[trackPieceIndex];

        if (currentTrackPiece.Angle != 0)
        {
            //this is a curve, so calculating next curve info makes no sense
            nextCurveDistance.Add(trackPieceIndex, 0);
            nextCurvePiece.Add(trackPieceIndex, trackPieceIndex);

            //calulates past straight line values
            double distance = 0;
            for (int i = trackPieceIndex-1; i >= lastStraightLineStart; i--)
            {
                //as all pieces before were straight lines, it is safe to get the first lane for distance
                distance += raceData.TrackPieces[i].Length[0];
                nextCurveDistance.Add(i, distance);
                nextCurvePiece.Add(i, trackPieceIndex);
            }

            lastStraightLineStart = trackPieceIndex + 1;
        }
    }

    public void AfterLoopProcess()
    {
        //calulates the last straight line values
        double distance = nextCurveDistance[0];
        for (int i = raceData.TrackPieces.Count - 1; i >= lastStraightLineStart; i--)
        {
            //as all pieces before were straight lines, it is safe to get the first lane for distance
            distance += raceData.TrackPieces[i].Length[0];
            nextCurveDistance.Add(i, distance);
            nextCurvePiece.Add(i, nextCurvePiece[0]);
        }

        raceData.LastStraightLineStart = lastStraightLineStart > 0 ? lastStraightLineStart : raceData.TrackPieces.Count;
        Console.WriteLine(string.Format("Last straight line start: {0}", raceData.LastStraightLineStart));
        for (int i = 0; i < raceData.TrackPieces.Count; i++)
        {
            raceData.NextCurvePiece.Add(nextCurvePiece[i]);
            raceData.NextCurveDistance.Add(nextCurveDistance[i]);
        }
    }
}
