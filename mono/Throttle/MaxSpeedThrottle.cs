using System;

class MaxSpeedThrottle: ThrottleHeuristic
{
    private RaceData raceData;

    public MaxSpeedThrottle(RaceData raceData)
    {
        this.raceData = raceData;
    }

    public double determineThrottle()
    {
        double currentSpeed = raceData.CurrentSpeed;
        int currentPieceIndex = raceData.CurrentTrackPiece;
        int startLaneIndex = raceData.CurrentTrackPieceStartLane;
        int endLaneIndex = raceData.CurrentTrackPieceEndLane;

        double maxSpeedCurrentPiece = 
            Math.Min(raceData.MaxSpeedPerPiecePerLane[currentPieceIndex][startLaneIndex], 
                     raceData.MaxSpeedPerPiecePerLane[currentPieceIndex][endLaneIndex]);

        double throttle;
        double? lastTickAngle = null;
        if (raceData.CarHistory.State.ContainsKey(raceData.GameTick - 1))
        {
            lastTickAngle = raceData.CarHistory.State[raceData.GameTick - 1].CurrentAngle;
        }

        TrackPiece currentTrackPiece = raceData.TrackPieces[currentPieceIndex];
        TrackPiece nextTrackPiece = raceData.TrackPieces[(currentPieceIndex + 1) % raceData.TrackPieces.Count];

        double distanceBeforeEndingPiece = (currentTrackPiece.Length[endLaneIndex] * Parameters.END_OF_PIECE_RATIO);
        bool isNextPieceStraight = nextTrackPiece.Angle == 0;
        /*
        bool isNextPieceOppositeAngle = (currentTrackPiece.Angle > 0 && nextTrackPiece.Angle < 0) || 
                                        (currentTrackPiece.Angle < 0 && nextTrackPiece.Angle > 0);
        */

        //this is the last straight line and we are on the last lap, so full throttle
        if (raceData.CurrentLap == raceData.RaceLaps - 1 && 
            raceData.CurrentTrackPiece >= raceData.LastStraightLineStart)
        {
            throttle = Parameters.MAX_THROTTLE;
        }
        //angle is high on a curve and has risk of crashing
        else if (currentTrackPiece.Angle != 0 && 
                 Math.Abs(raceData.CurrentAngle) > Parameters.MAX_CAR_ANGLE_BEFORE_CRASHING)
        {
            //angle is increasing and the curve is ending, throttle a bit to reduce angle
            if (lastTickAngle.HasValue && Math.Abs(raceData.CurrentAngle) > Math.Abs(lastTickAngle.Value) &&
                raceData.CurrentTrackInPieceDistance > distanceBeforeEndingPiece && isNextPieceStraight)
            {
                throttle = Parameters.INTERMEDIATE_THROTTLE;
            }
            //angle is not increasing, it's safer to reduce speed
            else
            {
                throttle = Parameters.MIN_THROTTLE;
            }
        }
        //angle is increasing/decreasing fast on a curve, also risk of crashing
        else if (lastTickAngle.HasValue && currentTrackPiece.Angle != 0 &&
                 Math.Abs(Math.Abs(raceData.CurrentAngle) - Math.Abs(lastTickAngle.Value)) > Parameters.MAX_ANGLE_INCREASE_ALLOWED)
        {
            //the curve is ending, throttle a bit to reduce angle
            if (raceData.CurrentTrackInPieceDistance > distanceBeforeEndingPiece && isNextPieceStraight)
            {
                throttle = Parameters.INTERMEDIATE_THROTTLE;
            }
            //angle is not increasing, it's safer to reduce speed
            else
            {
                throttle = Parameters.MIN_THROTTLE;
            }

            //warns the max speed reduction algorithm that max speed should be reduced
            raceData.MaxAngleInCurrentTrackPiece = Math.Max(raceData.MaxAngleInCurrentTrackPiece, 
                                                            Parameters.MAX_CAR_ANGLE_BEFORE_CRASHING + 1);
        }
        //risk of crashing, full deceleration
        else if (currentSpeed >= maxSpeedCurrentPiece)
        {
            throttle = Parameters.MIN_THROTTLE;
            //Console.WriteLine(string.Format("Current Speed: {0}|\tMax Speed on Current Piece: {1}", currentSpeed, maxSpeedCurrentPiece));
        }
        //car can accelerate based on the current piece... lets check more variables
        else
        {
            int nextSlowerPiece = raceData.NextSlowerPiece[currentPieceIndex];
            int nextSlowerPieceLane = GetNextSlowerPieceLane(currentPieceIndex, nextSlowerPiece, endLaneIndex);

            double ticksToReduceSpeed = CalculateTickToReduceSpeed(currentSpeed, nextSlowerPieceLane, nextSlowerPiece);
            double distanceUntilReduce = CalculateDistance(currentSpeed, ticksToReduceSpeed);
            double distanceToSlowerPiece = raceData.NextSlowerPieceDistance[currentPieceIndex][nextSlowerPieceLane] - raceData.CurrentTrackInPieceDistance;
            /*
            Console.WriteLine(string.Format(
                "Next: {0}|\tSpeed: {1}|\tDistance: {2}|\tSprint: {4}|\tTicks: {3}", 
                nextSlowerPiece, raceData.MaxSpeedPerPiecePerLane[nextSlowerPiece][nextSlowerPieceLane], 
                distanceToSlowerPiece, ticksToReduceSpeed, distanceUntilReduce));
            */

            //if approaching a piece that requires deceleration
            if (distanceUntilReduce >= distanceToSlowerPiece &&
                currentSpeed > raceData.MaxSpeedPerPiecePerLane[nextSlowerPiece][nextSlowerPieceLane])
            {
                //decelerates previously so does not crash
                throttle = Parameters.MIN_THROTTLE;
            }
            //peeks the slower piece after the slower next to this to see if needs to decelare too
            else
            {
                int afterNextSlowerPiece = raceData.NextSlowerPiece[nextSlowerPiece];
                int afterNextSlowerPieceLane = GetNextSlowerPieceLane(currentPieceIndex, afterNextSlowerPiece, nextSlowerPieceLane);
                ticksToReduceSpeed = CalculateTickToReduceSpeed(currentSpeed, afterNextSlowerPieceLane, afterNextSlowerPiece);
                distanceUntilReduce = CalculateDistance(currentSpeed, ticksToReduceSpeed);
                distanceToSlowerPiece += raceData.NextSlowerPieceDistance[nextSlowerPiece][afterNextSlowerPieceLane];

                /*
                Console.WriteLine(string.Format(
                    "ANext: {0}|\tASpeed: {1}|\tADistance: {2}|\tASprint: {4}|\tATicks: {3}", 
                    afterNextSlowerPiece, raceData.MaxSpeedPerPiecePerLane[afterNextSlowerPiece][afterNextSlowerPieceLane], 
                    distanceToSlowerPiece, ticksToReduceSpeed, distanceUntilReduce));
                */

                //if approaching a piece that requires deceleration
                if (distanceUntilReduce >= distanceToSlowerPiece &&
                    currentSpeed > raceData.MaxSpeedPerPiecePerLane[afterNextSlowerPiece][afterNextSlowerPieceLane])
                {
                    //decelerates previously so does not crash
                    throttle = Parameters.MIN_THROTTLE;
                }
                //the speed is good: try to keep a constant speed
                else
                {
                    throttle = Parameters.MAX_THROTTLE;
                }
            }
       }

        return throttle;
    }

    /// <summary>
    /// Gets the lane on which the car will be on next slower piece.
    /// </summary>
    /// <returns>The lane on which the car will be on next slower piece.</returns>
    /// <param name="currentPieceIndex">Current piece index.</param>
    /// <param name="nextSlowerPiece">Next slower piece index.</param>
    /// <param name="currentEndLane">End lane on current piece.</param>
    int GetNextSlowerPieceLane(int currentPieceIndex, int nextSlowerPiece, int currentEndLane)
    {
        int nextSlowerPieceLane = currentEndLane;

        //next switch is between current piece and next slower piece
        if (raceData.NextSwitchIndex <= nextSlowerPiece && raceData.NextSwitchIndex > currentPieceIndex)
        {
            nextSlowerPieceLane = raceData.NextSwitchEndLane;
        }
        //next switch is between current piece and next slower piece, which will be on next lap
        else if (raceData.NextSwitchIndex > currentPieceIndex && nextSlowerPiece < currentPieceIndex)
        {
            nextSlowerPieceLane = raceData.NextSwitchEndLane;
        }
        //next switch is between current piece and next slower piece, both being on next lap
        else if (raceData.NextSwitchIndex <= nextSlowerPiece && nextSlowerPiece < currentPieceIndex)
        {
            nextSlowerPieceLane = raceData.NextSwitchEndLane;
        }

        return nextSlowerPieceLane;
    }

    /// <summary>
    /// Calculate how many ticks needed until reaching the next slower piece speed
    /// </summary>
    /// <returns>The number of ticks estimated to reduce to acceptable speed.</returns>
    /// <param name="currentSpeed">Current speed.</param>
    /// <param name="endLaneIndex">End lane index.</param>
    /// <param name="slowerPieceIndex">Index of the referenced slower piece.</param>
    double CalculateTickToReduceSpeed(double currentSpeed, int endLaneIndex, int slowerPieceIndex)
    {
        return Parameters.TICKS_DELAY + Math.Log((
            raceData.MaxSpeedPerPiecePerLane[slowerPieceIndex][endLaneIndex] / currentSpeed), 
            Parameters.ATTRITION_RATE + 1);
    }

    /// <summary>
    /// Calculates the distance the bot runs decelerating from the current speed for a specific number of ticks.
    /// </summary>
    /// <returns>The distance the bot will run.</returns>
    /// <param name="curentSpeed">Curent speed.</param>
    /// <param name="ticks">Ticks to calculate distance.</param>
    double CalculateDistance(double curentSpeed, double ticks)
    {
        return (curentSpeed * (Math.Pow(Parameters.ATTRITION_RATE + 1, ticks) - 1)) / Parameters.ATTRITION_RATE;
    }
}