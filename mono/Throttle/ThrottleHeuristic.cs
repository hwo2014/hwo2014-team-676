using System;

public interface ThrottleHeuristic
{
    /// <summary>
    /// Determines the throttle value.
    /// </summary>
    /// <returns>The throttle value.</returns>
	double determineThrottle();
}
