using System;

public class DummyThrottle: ThrottleHeuristic
{
	public double determineThrottle() {
		return 0.55;
	}
}