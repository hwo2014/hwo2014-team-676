using System;
using Newtonsoft.Json.Linq;

static class CarPositions
{

    /// <summary>
    /// Process the specified msgData and raceData for the CarPositions msgType.
    /// </summary>
    /// <param name="msgData">Message data</param>
	/// <param name="raceData">Race data</param>
	internal static SendMsg process(string msgData, RaceData raceData, ThrottleHeuristic throttleHeuristic)
    {
        SendMsg returnMsg = null;
        JArray parsedJson = JArray.Parse(msgData);

        int myBotIndex = ResponseUtil.ExtractMyBotIndex(raceData, parsedJson);

        int currentPieceIndex = parsedJson[myBotIndex]["piecePosition"]["pieceIndex"].ToObject<int>();
        int startLaneIndex = parsedJson[myBotIndex]["piecePosition"]["lane"]["startLaneIndex"].ToObject<int>();
        int endLaneIndex = parsedJson[myBotIndex]["piecePosition"]["lane"]["endLaneIndex"].ToObject<int>();
        double currentInPieceDistance = parsedJson[myBotIndex]["piecePosition"]["inPieceDistance"].ToObject<double>();
        double currentSpeed = CalculateCurrentSpeed(raceData, parsedJson, myBotIndex, currentPieceIndex, 
                                                    currentInPieceDistance, startLaneIndex, endLaneIndex);
		double currentAngle = parsedJson[myBotIndex]["angle"].ToObject<double>();
		double throttle = Parameters.MIN_THROTTLE;

        ProcessMaxAngleOnPiece(raceData, currentPieceIndex, startLaneIndex, endLaneIndex, currentSpeed, currentAngle);
        PersistCurrentTickCompetitorsData(raceData, parsedJson, myBotIndex);
        ProcessTurboLogic(raceData, ref returnMsg, currentPieceIndex, currentInPieceDistance, currentSpeed, endLaneIndex);
        ProcessSwitchesLogic(raceData, ref returnMsg, currentPieceIndex);
        PersistCurrentTickRaceData(raceData, currentPieceIndex, startLaneIndex, endLaneIndex, currentInPieceDistance, currentSpeed, currentAngle);
        ProcessAttrition(raceData);
        ProcessThrottleLogic(raceData, throttleHeuristic, ref returnMsg, currentAngle, ref throttle);

		raceData.CarHistory.Memorize(raceData.GameTick, new MyCarState(raceData.GameTick, currentPieceIndex, currentInPieceDistance, throttle, currentSpeed, currentAngle));

        return returnMsg;
    }

    /// <summary>
    /// Calculates the current speed.
    /// </summary>
    /// <returns>The current speed</returns>
    /// <param name="raceData">Race data</param>
    /// <param name="parsedJson">Parsed json of the game message data</param>
    /// <param name="myBotIndex">My bot index</param>
    /// <param name="currentPieceIndex">Current piece index</param>
    /// <param name="currentInPieceDistance">Current in piece distance</param>
    private static double CalculateCurrentSpeed(RaceData raceData, JArray parsedJson, int myBotIndex, 
                                                int currentPieceIndex, double currentInPieceDistance,
                                                int startLaneIndex, int endLaneIndex)
    {
        double currentSpeed = 0.0;
        if (currentPieceIndex == raceData.CurrentTrackPiece)
        {
            currentSpeed = (currentInPieceDistance - raceData.CurrentTrackInPieceDistance) / (raceData.GameTick - raceData.CurrentTrackInPieceDistanceGameTick);
        }
        else
        {
            double lastPieceLength = ((raceData.TrackPieces[raceData.CurrentTrackPiece].Length[startLaneIndex]) + raceData.TrackPieces[raceData.CurrentTrackPiece].Length[endLaneIndex]) / 2;
            currentSpeed = ((lastPieceLength - raceData.CurrentTrackInPieceDistance) + currentInPieceDistance) / (raceData.GameTick - raceData.CurrentTrackInPieceDistanceGameTick);
        }

        //FIXME Debug to find out why currentSpeed is not correctly calculated
        return Math.Abs(currentSpeed);
    }

    /// <summary>
    /// Processes the max angle on piece.
    /// </summary>
    /// <param name="raceData">Race data.</param>
    /// <param name="currentPieceIndex">Current piece index.</param>
    /// <param name="currentSpeed">Current speed.</param>
    /// <param name="currentAngle">Current angle.</param>
    private static void ProcessMaxAngleOnPiece(RaceData raceData, int currentPieceIndex, int startLane, 
                                               int endLane, double currentSpeed, double currentAngle)
    {
        if (currentPieceIndex != raceData.CurrentTrackPiece)
        {
            if (raceData.TrackPieces[raceData.CurrentTrackPiece].Angle != 0)
            {
                double maxSpeedOnPieceLane;
                double maxSpeedMultiplier;

                for (int lane = Math.Min(startLane,endLane); lane <= Math.Max(startLane,endLane); lane++)
                {
                    maxSpeedOnPieceLane = raceData.MaxSpeedPerPiecePerLane[raceData.CurrentTrackPiece][lane];
                    maxSpeedMultiplier = 1;
                    // speed on piece was higher than max allowed: should it be increased?
                    if (raceData.MaxSpeedInCurrentTrackPiece >= maxSpeedOnPieceLane)
                    {
                        if (raceData.MaxAngleInCurrentTrackPiece < Parameters.ANGLE_THRESHOLD_FOR_INCREASING_SPEED_VERY_GREATLY)
                        {
                            maxSpeedMultiplier = Parameters.MAX_SPEED_INCREASE_RATIO_VERY_LOW_ANGLE;
                            Console.WriteLine(string.Format("Increasing very greatly max speed on piece {0} and lane {1}", raceData.CurrentTrackPiece, lane));
                        }
                        else if (raceData.MaxAngleInCurrentTrackPiece < Parameters.ANGLE_THRESHOLD_FOR_INCREASING_SPEED_GREATLY)
                        {
                            maxSpeedMultiplier = Parameters.MAX_SPEED_INCREASE_RATIO_LOW_ANGLE;
                            Console.WriteLine(string.Format("Increasing greatly max speed on piece {0} and lane {1}", raceData.CurrentTrackPiece, lane));
                        }
                        else if (raceData.MaxAngleInCurrentTrackPiece < Parameters.ANGLE_THRESHOLD_FOR_INCREASING_SPEED)
                        {
                            maxSpeedMultiplier = Parameters.MAX_SPEED_INCREASE_RATIO_MEDIUM_ANGLE;
                            Console.WriteLine(string.Format("Increasing moderately max speed on piece {0} and lane {1}", raceData.CurrentTrackPiece, lane));
                        }
                    }
                    // speed on piece was lower than max allowed: should it be reduced?
                    //else //if (raceData.MaxSpeedInCurrentTrackPiece < maxSpeedOnPiece)
                    {
                        if (raceData.MaxAngleInCurrentTrackPiece > Parameters.MAX_CAR_ANGLE_BEFORE_CRASHING)
                        {
                            maxSpeedMultiplier = Parameters.MAX_SPEED_DECREASE_RATIO_HIGH_ANGLE;
                            Console.WriteLine(string.Format("Decreasing greatly max speed on piece {0} and lane {1}", raceData.CurrentTrackPiece, lane));
                        }
                        else if (raceData.MaxAngleInCurrentTrackPiece > Parameters.ANGLE_THRESHOLD_FOR_DECREASING_SPEED)
                        {
                            maxSpeedMultiplier = Parameters.MAX_SPEED_DECREASE_RATIO_MEDIUM_ANGLE;
                            Console.WriteLine(string.Format("Decreasing max speed on piece {0} and lane {1}", raceData.CurrentTrackPiece, lane));
                        }
                    }
                    if (maxSpeedMultiplier != 1)
                    {
                        Console.WriteLine(string.Format(
                            "Max speed previously stored: {0}|\tMax speed achieved: {1}|\tMax angle achieved: {2}",
                            maxSpeedOnPieceLane, raceData.MaxSpeedInCurrentTrackPiece, raceData.MaxAngleInCurrentTrackPiece));
                        raceData.MaxSpeedPerPiecePerLane[raceData.CurrentTrackPiece][lane] *= maxSpeedMultiplier;
                    }
                }
            }
            raceData.MaxAngleInCurrentTrackPiece = 0;
            raceData.MaxSpeedInCurrentTrackPiece = 0;
        }
        else
        {
            raceData.MaxSpeedInCurrentTrackPiece = Math.Max(currentSpeed, raceData.MaxSpeedInCurrentTrackPiece);
            raceData.MaxAngleInCurrentTrackPiece = Math.Max(Math.Abs(currentAngle), raceData.MaxAngleInCurrentTrackPiece);
        }
    }

    /// <summary>
    /// Processes the turbo logic.
    /// </summary>
    /// <param name="raceData">Race data.</param>
    /// <param name="returnMsg">Return message.</param>
    /// <param name="currentPieceIndex">Current piece index.</param>
    /// <param name="currentInPieceDistance">Current in piece distance.</param>
    /// <param name="currentSpeed">Current speed.</param>
    static void ProcessTurboLogic(RaceData raceData, ref SendMsg returnMsg, int currentPieceIndex, 
                                  double currentInPieceDistance, double currentSpeed, int endLaneIndex)
    {
        string turboMessage = string.Empty;
        double fixedCurrentSpeed = currentPieceIndex == raceData.CurrentTrackPiece ?
            currentSpeed : raceData.CurrentSpeed;
        double distanceToNextCurve = raceData.NextCurveDistance[currentPieceIndex] - currentInPieceDistance;
        if (returnMsg == null && !raceData.TurboActive && raceData.TurboAvailable) 
        {
            // a turbo is available. Let's check if it's a good time to use it
            bool useTurbo = false;
            //this is the last straight line and we are on the last lap, so full throttle
            if (raceData.CurrentLap == raceData.RaceLaps - 1 && 
                raceData.CurrentTrackPiece >= raceData.LastStraightLineStart)
            {
                // we are in the last straight line
                useTurbo = true;
                turboMessage = "Last Sprint!!!";
            }
            else if ((currentPieceIndex == raceData.BestPieceIndexForTurbo || 
                     (raceData.TrackPieces[currentPieceIndex].Angle == 0 && 
                        fixedCurrentSpeed * (raceData.TurboDuration + 1) * raceData.TurboFactor < distanceToNextCurve)))
            {
                // we are either on the best piece of the track to use or we can use it safely before a curve
                useTurbo = true;
                turboMessage = "Banzai";
                /*
                Console.WriteLine(string.Format("GameTick: {0}", raceData.GameTick));
                Console.WriteLine(string.Format("currentPieceIndex: {0}|\tbestPieceIndexForTurbo: {1}",
                                                currentPieceIndex, raceData.BestPieceIndexForTurbo));
                Console.WriteLine(string.Format(
                    "currentPieceAngle: {0}|\tfixedCurrentSpeed: {1}|\tturboDuration: {2}|\tturboFactor: {3}",
                    raceData.TrackPieces[currentPieceIndex].Angle, fixedCurrentSpeed, raceData.TurboDuration,
                    raceData.TurboFactor));
                Console.WriteLine(string.Format(
                    "Multiplication: {0}|\tdistanceToNextCurve: {1}",
                    fixedCurrentSpeed * (raceData.TurboDuration + 1) * raceData.TurboFactor,
                    distanceToNextCurve));
                */
            }
            else if (raceData.OtherCars.Count > 0 && (raceData.TrackPieces[currentPieceIndex].Angle == 0 || 
                        raceData.TrackPieces[(currentPieceIndex + 1) % raceData.TrackPieces.Count].Angle == 0))
            {
                // There are other cars on the track and we are either on a curved piece or approaching one.
                // Check if there is a car in front of us
                foreach (CarState otherCar in raceData.OtherCars.Values)
                {
                    if (otherCar.Angle > Parameters.ANGLE_THRESHOLD_FOR_DECREASING_SPEED && 
                        otherCar.IsRightBeyond(currentPieceIndex, currentInPieceDistance, endLaneIndex, raceData.MyCar.GuideFlagPosition, raceData))
                    {
                        // our car is right before an enemy car with a high enough angle, on the same lane
                        // we can push another car to death
                        useTurbo = true;
                        turboMessage = "Push to Death!!";

                        Console.WriteLine(string.Format(
                            "Trying to push {0} car (angle = {1}) to death on tick {2}",
                            otherCar.Color, otherCar.Angle, raceData.GameTick));
                        /*
                        Console.WriteLine(string.Format("GameTick: {0}", raceData.GameTick));
                        Console.WriteLine(string.Format("otherCar: {0}|\totherCarAngle: {1}",
                                                        otherCar.Color, otherCar.Angle));
                        Console.WriteLine(string.Format(
                            "My car:\tcurrentPiece: {0}|\tcurrentInPieceDistance: {1}|\tendLane: {2}|\tmyFlagPosition: {3}",
                            currentPieceIndex, currentInPieceDistance, endLaneIndex, raceData.MyCar.GuideFlagPosition));
                        Console.WriteLine(string.Format(
                            "Other car:\tcurrentPiece: {0}|\tcurrentInPieceDistance: {1}|\tendLane: {2}|\tmyFlagPosition: {3}",
                            otherCar.PieceIndex, otherCar.InPieceDistance, otherCar.EndLaneIndex, otherCar.GuideFlagPosition));
                        */

                        break;
                    }
                }
            }

            if (useTurbo)
            {
                Console.WriteLine("Turbo: " + turboMessage);
                returnMsg = new Turbo(turboMessage);
                raceData.TurboAvailable = false;
            }
        }
    }

    /// <summary>
    /// Processes the switches logic.
    /// </summary>
    /// <param name="raceData">Race data.</param>
    /// <param name="returnMsg">Return message.</param>
    /// <param name="currentPieceIndex">Current piece index.</param>
    static void ProcessSwitchesLogic(RaceData raceData, ref SendMsg returnMsg, int currentPieceIndex)
    {
        if (returnMsg == null && raceData.CurrentTrackPiece != currentPieceIndex 
            && raceData.BestDirectionNextSwitch.ContainsKey(currentPieceIndex + 1))
        {
            string nextSwitchDirection = raceData.BestDirectionNextSwitch[currentPieceIndex + 1];
            returnMsg = new SwitchLane(nextSwitchDirection);

            raceData.NextSwitchIndex = raceData.BestDirectionNextSwitchIndex[currentPieceIndex + 1];
            raceData.NextSwitchEndLane = nextSwitchDirection.Equals("Right") ? 
                raceData.CurrentTrackPieceEndLane + 1 : 
                raceData.CurrentTrackPieceEndLane - 1;
            raceData.NextSwitchEndLane = Math.Max(raceData.NextSwitchEndLane, 0);
            raceData.NextSwitchEndLane = Math.Min(raceData.NextSwitchEndLane, raceData.LanesDistanceFromCenter.Count - 1);

            Console.WriteLine(string.Format("Next Switch: {0}|\tNextSwitchEndLane: {1}|\tDirection: {2}",
                                            raceData.NextSwitchIndex, raceData.NextSwitchEndLane, nextSwitchDirection));
        }
    }

    /// <summary>
    /// Persists the current tick race data.
    /// </summary>
    /// <param name="raceData">Race data.</param>
    /// <param name="currentPieceIndex">Current piece index.</param>
    /// <param name="startLaneIndex">Start lane index.</param>
    /// <param name="endLaneIndex">End lane index.</param>
    /// <param name="currentInPieceDistance">Current in piece distance.</param>
    /// <param name="currentSpeed">Current speed.</param>
    /// <param name="currentAngle">Current angle.</param>
    static void PersistCurrentTickRaceData(RaceData raceData, int currentPieceIndex, int startLaneIndex, 
                                           int endLaneIndex, double currentInPieceDistance, double currentSpeed, 
                                           double currentAngle)
    {
        raceData.CurrentSpeed = currentSpeed;
        if (currentSpeed > raceData.MaxSpeed)
        {
            raceData.MaxSpeed = currentSpeed;
        }
        raceData.CurrentTrackInPieceDistance = currentInPieceDistance;
        raceData.CurrentTrackInPieceDistanceGameTick = raceData.GameTick;
        raceData.CurrentTrackPiece = currentPieceIndex;
        raceData.CurrentTrackPieceStartLane = startLaneIndex;
        raceData.CurrentTrackPieceEndLane = endLaneIndex;
        raceData.CurrentAngle = currentAngle;
    }

    /// <summary>
    /// Persists the current tick competitors data.
    /// </summary>
    /// <param name="raceData">Race data.</param>
    /// <param name="parsedMessageJson">Parsed message json.</param>
    /// <param name="myBotIndex">My bot index.</param>
    static void PersistCurrentTickCompetitorsData(RaceData raceData, JArray parsedMessageJson, int myBotIndex)
    {
        for (int i = 0; i < parsedMessageJson.Count; i++)
        {
            if (i != myBotIndex)
            {
                JToken otherCarData = parsedMessageJson[i];
                string botKey = string.Format("{0}-{1}", otherCarData["id"]["name"].ToObject<string>(), otherCarData["id"]["color"].ToObject<string>());
                if (raceData.OtherCars.ContainsKey(botKey))
                {
                    CarState otherCar = raceData.OtherCars[botKey];
                    otherCar.Angle = otherCarData["angle"].ToObject<double>();
                    otherCar.PieceIndex = otherCarData["piecePosition"]["pieceIndex"].ToObject<int>();
                    otherCar.EndLaneIndex = otherCarData["piecePosition"]["lane"]["endLaneIndex"].ToObject<int>();
                    otherCar.Lap = otherCarData["piecePosition"]["lap"].ToObject<int>();
                }
            }
        }
    }

    /// <summary>
    /// Processes the attrition rate.
    /// </summary>
    /// <param name="raceData">Race data.</param>
    static void ProcessAttrition(RaceData raceData)
    {
        if (raceData.AttritionRateCalculated || !raceData.CarHistory.State.ContainsKey(raceData.GameTick - 1))
        {
            return;
        }

        MyCarState lastTickState = raceData.CarHistory.State[raceData.GameTick - 1];

        if (lastTickState.Throttle == Parameters.MIN_THROTTLE &&
            raceData.CurrentTrackInPieceDistance > raceData.CurrentSpeed &&
            lastTickState.CurrentTrackInPieceDistance > lastTickState.CurrentSpeed)
        {
            double calculatedAttrition = 
                Math.Round((raceData.CurrentSpeed - lastTickState.CurrentSpeed) / lastTickState.CurrentSpeed, 6);

            // makes no sense, and do not continue in order to avoid a bug
            if (calculatedAttrition > 0)
            {
                return;
            }

            //Console.WriteLine(string.Format("Current Attrition: {0}|\tCalculated Attrition: {1}", Parameters.ATTRITION_RATE, calculatedAttrition));

            int count = raceData.AttritionRateCounter.ContainsKey(calculatedAttrition) ?
                (raceData.AttritionRateCounter[calculatedAttrition] + 1) : 1;

            if (count >= Parameters.VALUES_NEEDED_FOR_ATTRITION_CONSISTENCY)
            {
                raceData.AttritionRateCalculated = true;
                Parameters.ATTRITION_RATE = calculatedAttrition;
                raceData.AttritionRateCounter.Clear();
                Console.WriteLine(string.Format("Attrition rate changing to {0}", calculatedAttrition));
            }

            raceData.AttritionRateCounter[calculatedAttrition] = count;
        }
    }

    /// <summary>
    /// Processes the throttle logic.
    /// </summary>
    /// <param name="raceData">Race data.</param>
    /// <param name="throttleHeuristic">Throttle heuristic.</param>
    /// <param name="returnMsg">Return message.</param>
    /// <param name="currentAngle">Current angle.</param>
    /// <param name="throttle">Throttle.</param>
    static void ProcessThrottleLogic(RaceData raceData, ThrottleHeuristic throttleHeuristic, ref SendMsg returnMsg, 
                                     double currentAngle, ref double throttle)
    {
        if (returnMsg == null)
        {
            throttle = throttleHeuristic.determineThrottle();
            Console.WriteLine(string.Format("{0}|\t{1}|\t{2}|\t{3}|\t{4}|\t{5}", raceData.GameTick, raceData.CurrentTrackPiece, raceData.CurrentTrackInPieceDistance, throttle, raceData.CurrentSpeed, currentAngle));
            returnMsg = new Throttle(throttle);
        }
    }
}
