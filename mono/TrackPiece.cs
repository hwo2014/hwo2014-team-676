using System;
using System.Collections.Generic;

public class TrackPiece
{
    public List<double> Length { get; private set; }
    public bool Switch { get; private set; }
    public List<double> Radius { get; private set; }
    public double Angle { get; private set; }
	public bool IsDangerous { get; private set; }
	public List<TrackPieceTag> Tags { get; private set; }

    public TrackPiece(double length, bool hasSwitch, double radius, double angle, List<int> lanesDistance)
    {
        this.Length = new List<double>(lanesDistance.Count);
        this.Switch = hasSwitch;
        this.Radius = new List<double>(lanesDistance.Count);
        this.Angle = angle;

        for (int i = 0; i < lanesDistance.Count; i++)
        {
            if (angle == 0)
            {
                this.Length.Add(length);
                this.Radius.Add(0.0);
            }
            else
            {
                this.Radius.Add(angle > 0 ? radius - lanesDistance[i] : radius + lanesDistance[i]);
                this.Length.Add(getPieceLength(angle, this.Radius[i]));
            }
        }

        this.IsDangerous = this.IsTrackPieceDangerous();
		this.Tags = new List<TrackPieceTag>();
    }

    private double getPieceLength(double angle, double radius)
    {
        return Math.Abs(angle / 360) * Math.PI * (radius * 2);
    }

	/// <summary>
	/// Determines if a track piece is a dangerous curve.
	/// A track piece is a dangerous curve if it has a small radius and a big angle at the same time.
	/// </summary>
	/// <returns><c>true</c> if is track piece dangerous; otherwise, <c>false</c>.</returns>
	private bool IsTrackPieceDangerous() {
		// TODO: always using the radius of the first lane. This might be a reasonable approximation.
		return (this.Radius[0] < Parameters.MIN_SAFE_RADIUS_FOR_TRACK_PIECE_WITH_BIG_ANGLE
		        && Math.Abs(this.Angle) > Parameters.MAX_SAFE_ANGLE_FOR_TRACK_PIECE_WITH_SHORT_RADIUS);
	}
  }
