using System;

static class GameEnd
{
    internal static SendMsg process(string msgData, RaceData raceData)
    {
		//Console.WriteLine("Race ended. My car state info below:");
		//PrintMyCarStateHistory(raceData);
        return Ping.One;
    }

    /// <summary>
    /// Prints the state information on every tick of our car.
    /// </summary>
    /// <param name="raceData">Race data.</param>
    static void PrintMyCarStateHistory(RaceData raceData)
    {
        for (long i = 0; i < raceData.GameTick; i++)
        {
            if (raceData.CarHistory.State.ContainsKey(i))
            {
                MyCarState state = raceData.CarHistory.State[i];
                Console.WriteLine(string.Format("{0}|\t{1}|\t{2:N13}|\t{3}|\t{4:N13}|\t{5:N15}", state.GameTick, state.CurrentTrackPiece, state.CurrentTrackInPieceDistance, state.Throttle, state.CurrentSpeed, state.CurrentAngle));
            }
        }
    }
}
