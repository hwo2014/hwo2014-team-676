using System;
using System.Collections.Generic;

public class CarState
{
    public string Name { get; private set; }
    public string Color { get; private set; }
    public double Length { get; private set; }
    public double GuideFlagPosition { get; private set; }

    public int Lap { get; set; }
    public int PieceIndex { get; set; }
    public double InPieceDistance { get; set; }
    public int EndLaneIndex { get; set; }
    public double Angle { get; set; }

    public CarState(string name, string color, double length, double guideFlagPosition)
    {
        this.Name = name;
        this.Color = color;
        this.Length = length;
        this.GuideFlagPosition = guideFlagPosition;
    }

    //FIXME this method should consider start and end lane, and since when they will be in the same lane
    public bool IsRightBeyond(int targetPieceIndex, double targetInPieceDistance, int targetEndLaneIndex, double targetGuideFlagPosition, RaceData raceData)
    {
        int piecesApart = Math.Abs(PieceIndex - targetPieceIndex) % raceData.TrackPieces.Count;
        if (targetPieceIndex > PieceIndex)
        {
            // target is ahead, so this car cannot be beyond
            return false;
        }
        else if (targetPieceIndex == PieceIndex && targetInPieceDistance > InPieceDistance)
        {
            // target is also ahead, so this car cannot be beyond
            return false;
        }
        if (piecesApart > 1)
        {
            // they are apart by more than 1 track piece.
            return false;
        }
        if (targetEndLaneIndex != EndLaneIndex)
        {
            // they are not in the same lanes
            return false;
        }
        // gets the total length between both cars
        double totalDistance = 0;
        // sums the length of all pieces between them
        for (int i = 0; i <= piecesApart; i++)
        {
            int current = (targetPieceIndex + i) % raceData.TrackPieces.Count;
            totalDistance += raceData.TrackPieces[current].Length[targetEndLaneIndex];
        }
        // removes the target car length and inpiecedistance
        totalDistance -= targetInPieceDistance + targetGuideFlagPosition;

        // removes this car length and distance to finish its current piece
        totalDistance -= (raceData.TrackPieces[PieceIndex].Length[EndLaneIndex] - InPieceDistance) + (Length - GuideFlagPosition);

        double reachableDistanceWithTurbo = raceData.TurboFactor * raceData.TurboDuration *
            (raceData.MaxSpeedPerPiecePerLane[PieceIndex][EndLaneIndex] - raceData.CurrentSpeed);
        return totalDistance < reachableDistanceWithTurbo;
    }
}

