using System;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

public class Bot {

	private StreamWriter writer;
    private RaceData raceData = new RaceData();
    private ThrottleHeuristic throttleHeuristic;

	Bot(StreamReader reader, StreamWriter writer, RaceBuilder raceBuilder) {
        throttleHeuristic = new MaxSpeedThrottle(raceData);

        this.writer = writer;
		string line;

		send(raceBuilder);
		DateTime startTime = DateTime.Now;

		HashSet<string> firstMessageTypes = new HashSet<string>();
		while((line = reader.ReadLine()) != null) {
			MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);

            JObject fullMessage = JObject.Parse(line);
            if (fullMessage["gameTick"] != null)
            {
                raceData.GameTick = fullMessage["gameTick"].ToObject<long>();
            }

            #region First Messages Log
			if (!firstMessageTypes.Contains(msg.msgType))
			{
				firstMessageTypes.Add(msg.msgType);
				Console.WriteLine("FIRST MESSAGE OF THE TYPE:");
				Console.WriteLine(msg.msgType);
				if (msg.data != null)
				{
					Console.WriteLine(msg.data.ToString());
				}
			}
            #endregion

            switch(msg.msgType) {
                case "carPositions":
                    send(CarPositions.process(msg.data.ToString(), raceData, throttleHeuristic));
					break;
                case "lapFinished":
                    send(LapFinished.process(msg.data.ToString(), raceData));
					break;
                case "turboAvailable":
                    send(TurboAvailable.process(msg.data.ToString(), raceData));
                    break;
                case "turboStart":
                    send(TurboStart.process(msg.data.ToString(), raceData));
                    break;
                case "turboEnd":
                    send(TurboEnd.process(msg.data.ToString(), raceData));
                    break;
                case "crash":
                    send(Crash.process(msg.data.ToString(), raceData));
                    break;
                case "spawn":
                    send(Spawn.process(msg.data.ToString(), raceData));
                    break;
				case "join":
					Console.WriteLine("Joined");
					send(Ping.One);
					break;
				case "createRace":
					Console.WriteLine("msgType createRace");
                    send(Ping.One);
					break;
				case "yourCar":
					send(YourCar.process(msg.data.ToString(), raceData));
					break;
				case "gameInit":
                    send(GameInit.process(msg.data.ToString(), raceData, send));
					break;
				case "gameEnd":
					send(GameEnd.process("", raceData));
					break;
				case "gameStart":
					send(GameStart.process("", raceData));
					break;
				case "finish":
					Console.WriteLine("msgType: finish");
                    send(Ping.One);
					break;
				case "tournamentEnd":
					Console.WriteLine("msgType: tournamentEnd");
                    send(Ping.One);
					break;
				default:
					Console.WriteLine ("Unknown msgType: " + msg.msgType);
                    send(Ping.One);
					break;
			}
		}

		DateTime endTime = DateTime.Now;
		Console.WriteLine ("Time running: " + endTime.Subtract(startTime));
	}

    private void send(SendMsg msg) {
		writer.WriteLine(msg.ToJson());
	}

    #region Main

    public static void Main(string[] args) {
        string host = args[0];
        int port = int.Parse(args[1]);
        string botName = args[2];
        string botKey = args[3];
		RaceBuilder raceBuilder = new RaceBuilder(args);

        Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);
		Console.WriteLine("Race type (from args): " + raceBuilder.ToString());

        using(TcpClient client = new TcpClient(host, port)) {
            NetworkStream stream = client.GetStream();
            StreamReader reader = new StreamReader(stream);
            StreamWriter writer = new StreamWriter(stream);
            writer.AutoFlush = true;

			new Bot(reader, writer, raceBuilder);
        }
    }

    #endregion
}
