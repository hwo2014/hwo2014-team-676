using System;

class SwitchLane: SendMsg {
    public string value;

    public SwitchLane(string value) {
        this.value = value;
    }

    protected override Object MsgData() {
        return this.value;
    }

    protected override string MsgType() {
        return "switchLane";
    }
}