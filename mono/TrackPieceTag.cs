using System;

public enum TrackPieceTag
{
	LAST_SPRINT_BEFORE_LAP_FINISHES,
	BEWARE_OF_THE_DANGEROUS_ZONE_AHEAD,
	LONG_SAFE_LINE_AHEAD
}
