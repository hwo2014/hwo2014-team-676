using System;

public class TrackSegment
{
	public int StartingTrackPieceIndex { get; set; }
	public int EndingTrackPieceIndex { get; set; }
	public double Length { get; set; }
	public int LengthInPieces {
		get {
			return EndingTrackPieceIndex - StartingTrackPieceIndex + 1;
		}
	}
    public bool Merged { get; set; }
    public bool IsDangerous { get; set; }

	public TrackSegment (int startingTrackPieceIndex)
	{
		this.StartingTrackPieceIndex = startingTrackPieceIndex;
		this.Length = 0.0;
	}
}
