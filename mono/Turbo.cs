using System;

class Turbo: SendMsg {
    private string msg = string.Empty;

    public Turbo() {
    }

    public Turbo(string msg)
    {
        this.msg = msg;
    }

    protected override Object MsgData() {
        string data = msg != string.Empty ? msg : "Banzai!";
        return data;
    }

    protected override string MsgType() {
        return "turbo";
    }
}
