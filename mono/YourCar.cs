using System;
using Newtonsoft.Json.Linq;

static class YourCar
{
    internal static SendMsg process(string msgData, RaceData raceData)
    {
        JObject parsedJson = JObject.Parse(msgData);
        raceData.BotColor = parsedJson["color"].ToObject<string>();
        raceData.BotName = parsedJson["name"].ToObject<string>();
        Console.WriteLine("Car: {0} ({1})", raceData.BotName, raceData.BotColor);

        return Ping.One;
    }
}