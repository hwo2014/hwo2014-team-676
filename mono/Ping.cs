using System;

class Ping: SendMsg {
    public static Ping One = new Ping();

    private Ping()
    {
    }

    protected override string MsgType() {
        return "ping";
    }
}
