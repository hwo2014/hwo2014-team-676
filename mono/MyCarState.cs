using System;

public class MyCarState
{
    //raceData.GameTick, raceData.CurrentTrackPiece, raceData.CurrentTrackInPieceDistance, throttle, raceData.CurrentSpeed, currentAngle));

    public long GameTick { get; private set; }
    public int CurrentTrackPiece { get; private set; }
    public double CurrentTrackInPieceDistance { get; private set; }
    public double Throttle { get; private set; }
    public double CurrentSpeed { get; private set; }
    public double CurrentAngle { get; private set; }

    public MyCarState(long gameTick, int currentTrackPiece, double currentTrackInPieceDistance, double throttle,
                      double currentSpeed, double currentAngle)
    {
        this.GameTick = gameTick;
        this.CurrentTrackPiece = currentTrackPiece;
        this.CurrentTrackInPieceDistance = currentTrackInPieceDistance;
        this.Throttle = throttle;
        this.CurrentSpeed = currentSpeed;
        this.CurrentAngle = currentAngle;
    }
}

