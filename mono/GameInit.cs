using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

static class GameInit
{
    public delegate void Send(SendMsg msg);

    static bool hasTrackBeenProcessed = false;

    /// <summary>
    /// Process the specified msgData and raceData for the GameInit msgType.
    /// </summary>
    /// <param name="msgData">Message data</param>
    /// <param name="raceData">Race data</param>
    internal static SendMsg process(string msgData, RaceData raceData, Send sendMsgDelegate)
    {
        Console.WriteLine("Race init");

		// we only process the track once - in case of qualifying rounds, we don't do it twice
        if (!hasTrackBeenProcessed)
        {
            JObject parsedJson = JObject.Parse(msgData);

            /* 
             * The ProcessTrack segment can take a lot of time (considering a real time response).
             * We are sending a throttle message right before and right after it so the bot does
             * not timeout. The DateTimes here are just for logging purpose and weren't removed
             * because it's important to keep an eye on them on every major change.
             * 
             * A benefit of this is that the bot is starting to accelerate before it would normally,
             * resulting in a speed up to 15% higher in the first tick.
             */
            DateTime initialTime = DateTime.Now;
            ParseLanes(raceData, parsedJson);
            DateTime afterParseLanes = DateTime.Now;
            ParseTrackPieces(raceData, parsedJson);
            DateTime afterParseTrackPieces = DateTime.Now;
            ParseRace(raceData, parsedJson);
            DateTime afterParseRace = DateTime.Now;
            sendMsgDelegate(new Throttle(1.0));
            ProcessTrack(raceData);
            sendMsgDelegate(new Throttle(1.0));
            DateTime afterProcessTrack = DateTime.Now;
            ProcessCars(raceData, parsedJson);
            DateTime afterProcessCars = DateTime.Now;

            Console.WriteLine(string.Format("ParseLanes: {0}", (afterParseLanes - initialTime).TotalMilliseconds));
            Console.WriteLine(string.Format("ParseTrackPieces: {0}", (afterParseTrackPieces - afterParseLanes).TotalMilliseconds));
            Console.WriteLine(string.Format("ParseRace: {0}", (afterParseRace - afterParseTrackPieces).TotalMilliseconds));
            Console.WriteLine(string.Format("ProcessTrack: {0}", (afterProcessTrack - afterParseRace).TotalMilliseconds));
            Console.WriteLine(string.Format("ProcessCars: {0}", (afterProcessCars - afterProcessTrack).TotalMilliseconds));

            hasTrackBeenProcessed = true;
        }
        // end of qualifying - reset raceData status values
        else
        {
            raceData.ResetStatusValues();

        }
        return Ping.One;
    }

    /// <summary>
    /// Parses the lanes.
    /// </summary>
    /// <param name="raceData">Race data</param>
    /// <param name="parsedJson">Parsed json</param>
    private static void ParseLanes(RaceData raceData, JObject parsedJson)
    {
        JArray trackLanes = (JArray)parsedJson["race"]["track"]["lanes"];
        Dictionary<int, int> lanesDistanceFromCenter = new Dictionary<int, int>();
        foreach (JToken laneJson in trackLanes)
        {
            lanesDistanceFromCenter.Add(laneJson["index"].ToObject<int>(), 
                                        laneJson["distanceFromCenter"].ToObject<int>());
        }
        for (int i = 0; i < lanesDistanceFromCenter.Count; i++)
        {
            raceData.LanesDistanceFromCenter.Add(lanesDistanceFromCenter[i]);
        }
    }

    /// <summary>
    /// Parses the track pieces.
    /// </summary>
    /// <param name="raceData">Race data</param>
    /// <param name="parsedJson">Parsed json</param>
    private static void ParseTrackPieces(RaceData raceData, JObject parsedJson)
    {
        JArray trackPiecesJson = (JArray)parsedJson["race"]["track"]["pieces"];
        foreach (JToken trackPieceJson in trackPiecesJson)
        {
            double length = trackPieceJson["length"] == null ? 0 : trackPieceJson["length"].ToObject<double>();
            bool hasSwitch = trackPieceJson["switch"] == null ? false : trackPieceJson["switch"].ToObject<bool>();
            double radius = trackPieceJson["radius"] == null ? 0 : trackPieceJson["radius"].ToObject<double>();
            double angle = trackPieceJson["angle"] == null ? 0 : trackPieceJson["angle"].ToObject<double>();
            raceData.TrackPieces.Add(new TrackPiece(length, hasSwitch, radius, angle, raceData.LanesDistanceFromCenter));
        }
    }

    /// <summary>
    /// Parses the race.
    /// </summary>
    /// <param name="raceData">Race data.</param>
    /// <param name="parsedJson">Parsed json.</param>
    private static void ParseRace(RaceData raceData, JObject parsedJson)
    {
        JObject raceSession = (JObject)parsedJson["race"]["raceSession"];
        JToken laps = (JToken)raceSession.SelectToken("laps");
        if (laps != null)
        {
            raceData.RaceLaps = laps.ToObject<int>();
        }
    }

    /// <summary>
    /// Processes the track for extracting initial game heuristic values.
    /// </summary>
    /// <param name="raceData">Race data.</param>
    private static void ProcessTrack(RaceData raceData)
    {
        //IMPORTANT: the processor will run in the exact order they are added to the list
        List<IGameInitProcessor> gameInitProcessors = new List<IGameInitProcessor>()
        {
            new BestDirectionForSwitches(raceData),
            new MaxSpeedPerTrackPiece(raceData),
            new NextCurves(raceData),
            new NextSlowerPiece(raceData),
            new MacroSegmentsDeterminer(raceData),
            new MacroSegmentsAndPiecesClassifier(raceData)
        };

        for (int i = 0; i < raceData.TrackPieces.Count; i++)
        {
            for (int j = 0; j < gameInitProcessors.Count; j++) {
                gameInitProcessors[j].ProcessCurrentPiece(i);
            }
        }

        for (int j = 0; j < gameInitProcessors.Count; j++) {
            gameInitProcessors[j].AfterLoopProcess();
        }
    }

    /// <summary>
    /// Processes the competitor cars and saves their information (name, color) for future use.
    /// </summary>
    /// <param name="raceData">Race data.</param>
    /// <param name="parsedJson">Parsed json.</param>
    private static void ProcessCars(RaceData raceData, JObject parsedJson)
    {
        JArray carsData = (JArray)parsedJson["race"]["cars"];
        for (int i = 0; i < carsData.Count; i++)
        {
            JObject carData = (JObject)carsData[i];
            string name = carData["id"]["name"].ToObject<string>();
            string color = carData["id"]["color"].ToObject<string>();
            double length = carData["dimensions"]["length"].ToObject<double>();
            double guideFlagPosition = carData["dimensions"]["guideFlagPosition"].ToObject<double>();
            if (!name.Equals(raceData.BotName) || !color.Equals(raceData.BotColor))
            {
                string botKey = string.Format("{0}-{1}", name, color);
                if (!raceData.OtherCars.ContainsKey(botKey))
                {
                    raceData.OtherCars.Add(botKey, new CarState(name, color, length, guideFlagPosition));
                }
            }
            else
            {
                raceData.MyCar = new CarState(name, color, length, guideFlagPosition);
            }

        }

    }
}
