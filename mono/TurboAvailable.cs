using System;
using Newtonsoft.Json.Linq;

static class TurboAvailable
{
    /// <summary>
    /// Process the specified msgData and raceData for the TurboAvailable msgType.
    /// </summary>
    /// <param name="msgData">Message data</param>
    /// <param name="raceData">Race data</param>
    internal static SendMsg process(string msgData, RaceData raceData)
    {
        // TODO this information can be used to to know other cars have turbo available
        if (!raceData.IsCarCrashed)
        {
            JObject parsedJson = JObject.Parse(msgData);
            raceData.TurboAvailable = true;
            raceData.TurboDuration = parsedJson["turboDurationTicks"].ToObject<int>();
            raceData.TurboFactor = parsedJson["turboFactor"].ToObject<double>();
        }

        Console.WriteLine(string.Format("Turbo Available: {0} ticks & factor of {1}",
                                        raceData.TurboDuration, raceData.TurboFactor));

        return Ping.One;
    }
}
