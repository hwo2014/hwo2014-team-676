using System;

static class Parameters
{
    #region Throttle Constants

    /// <summary>
    /// Maximum throttle value.
    /// </summary>
    public const double MAX_THROTTLE = 1.0;

    /// <summary>
    /// Intermediate throttle value.
    /// </summary>
    public const double INTERMEDIATE_THROTTLE = 0.75;

    /// <summary>
    /// Minimum throttle value.
    /// </summary>
    public const double MIN_THROTTLE = 0.0;

    #endregion

    #region General Bot Constants

    /// <summary>
    /// Maximum speed allowed for the car.
    /// </summary>
    public const double MAX_SPEED = 1000.0;

    /// <summary>
    /// Maximum initial speed allowed for the curves.
    /// </summary>
    public const double MAX_INITIAL_SPEED_CURVE = 9.5;

    /// <summary>
    /// Maximum angular speed.
    /// Value used on qualifyings: 0.0095
    /// Tested without crash: 0.0105
    /// Possible value obtained with calculations: 0.01154
    /// </summary>
    public const double MAX_ANGULAR_SPEED = 0.0110;

    /// <summary>
    /// Attrition rate of the track
    /// Value used on qualifyings: -0.02
    /// </summary>
    public static double ATTRITION_RATE = -0.02;

    /// <summary>
    /// Number of repeated values needed for attrition consistency
    /// </summary>
    public const int VALUES_NEEDED_FOR_ATTRITION_CONSISTENCY = 10;

    /// <summary>
    /// Ticks considered as a delay of the server response time
    /// </summary>
    public const int TICKS_DELAY = 1;

    #endregion

    #region Angles / Max Speed Feedback

    /// <summary>
    /// The maximum angle the car can achieve before crashing
    /// </summary>
    public static double MAX_CAR_ANGLE_POSSIBLE = 60;

    /// <summary>
    /// The maximum angle considered safe for the car just before crashing
    /// </summary>
    public static double MAX_CAR_ANGLE_BEFORE_CRASHING = 52;

    /// <summary>
    /// The maximum angle for considering the curve can be made a lot faster
    /// </summary>
    public static double ANGLE_THRESHOLD_FOR_INCREASING_SPEED_VERY_GREATLY = 5;

    /// <summary>
    /// The maximum angle for considering the curve can be made a faster
    /// </summary>
    public static double ANGLE_THRESHOLD_FOR_INCREASING_SPEED_GREATLY = 25;

    /// <summary>
    /// The maximum angle for considering the curve can be made a bit faster
    /// </summary>
    public static double ANGLE_THRESHOLD_FOR_INCREASING_SPEED = 38;

    /// <summary>
    /// The minimum angle for considering the curve should be made slower
    /// </summary>
    public static double ANGLE_THRESHOLD_FOR_DECREASING_SPEED = 48;

    /// <summary>
    /// The maximum angle increase considered safe.
    /// Values tested with good response: 2.5 and 3.0
    /// </summary>
    public const double MAX_ANGLE_INCREASE_ALLOWED = 2.5;

    /// <summary>
    /// Maximum speed multiplication factor when angle is medium in piece
    /// </summary>
    public const double MAX_SPEED_DECREASE_RATIO_MEDIUM_ANGLE = 0.95;

    /// <summary>
    /// Maximum speed multiplication factor when angle is too low in piece
    /// </summary>
    public const double MAX_SPEED_DECREASE_RATIO_HIGH_ANGLE = 0.9;

    /// <summary>
    /// Maximum speed multiplication factor when angle is very safely low in piece
    /// </summary>
    public const double MAX_SPEED_INCREASE_RATIO_VERY_LOW_ANGLE = 1.15;

    /// <summary>
    /// Maximum speed multiplication factor when angle is safely low in piece
    /// </summary>
    public const double MAX_SPEED_INCREASE_RATIO_LOW_ANGLE = 1.1;

    /// <summary>
    /// Maximum speed multiplication factor when angle is medium in piece
    /// </summary>
    public const double MAX_SPEED_INCREASE_RATIO_MEDIUM_ANGLE = 1.05;

    /// <summary>
    /// The ratio over which the piece is to be considered "finishing"
    /// </summary>
    public const double END_OF_PIECE_RATIO = 0.75;

    #endregion

    #region Macro Segment Constants

	/// <summary>
	/// The maximum value of angle for a track piece that has a short radius.
	/// If a track piece has a short radius and an angle higher than this value, it is
	/// considered a dangerous track piece
	/// </summary>
	public const double MAX_SAFE_ANGLE_FOR_TRACK_PIECE_WITH_SHORT_RADIUS = 23.0;

	/// <summary>
	/// The minimum value of radius for a track piece that has a big angle value.
	/// If a track piece has a big angle and a radius smaller than this value, it is
	/// considered a dangerous track piece
	/// </summary>
    public const double MIN_SAFE_RADIUS_FOR_TRACK_PIECE_WITH_BIG_ANGLE = 121.0;

	/// <summary>
	/// The minimum value of radius for a track piece that has a big angle value.
	/// If a track piece has a big angle and a radius smaller than this value, it is
	/// considered a dangerous track piece
	/// </summary>
	public const double STRICTER_MIN_SAFE_RADIUS_FOR_TRACK_PIECE_WITH_BIG_ANGLE = 51.0;

    #endregion
}
