using System;
using System.Collections.Generic;

public class RaceData
{
    #region Attributes

    private int? _bestPieceIndexForTurbo = null;

    #endregion

    #region Game/Track/Segment Properties

    public string BotName { get; set; }
    public string BotColor { get; set; }
    public double GuideFlagPosition { get; set; }
    public List<int> LanesDistanceFromCenter { get; private set; }
    public int RaceLaps { get; set; }
    public List<TrackPiece> TrackPieces { get; private set; }
    public List<TrackSegment> TrackSegments { get; private set; }
    public List<int> NextCurvePiece { get; private set; }
    public List<double> NextCurveDistance { get; private set; }
    public int LastStraightLineStart { get; set; }
    public List<int> NextSlowerPiece { get; private set; }
    public List<List<double>> NextSlowerPieceDistance { get; set; }

    #endregion
    #region Status/Mutable Properties

    public long GameTick { get; set; }
    public int CurrentLap { get; set; }
    public bool IsCarCrashed { get; set; }
    public bool TurboAvailable { get; set; }
    public int TurboDuration { get; set; }
    public double TurboFactor { get; set; }
    public bool TurboActive { get; set; }
    public int CurrentTrackPiece { get; set; }
    public int CurrentTrackPieceStartLane { get; set; }
    public int CurrentTrackPieceEndLane { get; set; }
    public double CurrentTrackInPieceDistance { get; set; }
    public long CurrentTrackInPieceDistanceGameTick { get; set; }
    public double CurrentAngle { get; set; }
    public double CurrentSpeed { get; set; }
    public double MaxSpeed { get; set; }
    public double MaxAngleInCurrentTrackPiece { get; set; }
    public double MaxSpeedInCurrentTrackPiece { get; set; }
    public int NextSwitchIndex { get; set; }
    public int NextSwitchEndLane { get; set; }

    #endregion
    #region Other cars

    public CarState MyCar { get; set; }
    public Dictionary<string, CarState> OtherCars { get; private set; }

    #endregion
    #region Logic Properties

    public int BestPieceIndexForTurbo
    {
        get 
        {
            if (!_bestPieceIndexForTurbo.HasValue)
            {
                _bestPieceIndexForTurbo = FindBestPieceIndexForTurbo();
            }
            return _bestPieceIndexForTurbo.Value;
        }
    }

    /// <summary>
    /// Gets or sets the best lane to change in the switch piece.
    /// Key is switch track piece index and value is the direction to take
    /// </summary>
    /// <value>The best direction to switch</value>
    public Dictionary<int, string> BestDirectionNextSwitch { get; private set; }
    public string FirstSwitchDirection { get; set; }
    /// <summary>
    /// Gets or sets the next switch index from the current switch.
    /// Key is switch track piece index and value is the next switch
    /// </summary>
    /// <value>The next switch where a direction change will be taken</value>
    public Dictionary<int, int> BestDirectionNextSwitchIndex { get; private set; }

    /// <summary>
    /// The Max Speed for every piece track (first index) for each lane (second index)
    /// </summary>
    /// <value>The max speed per piece per lane.</value>
    public List<List<double>> MaxSpeedPerPiecePerLane { get; set; }

    /// <summary>
    /// The car history.
    /// </summary>
    public CarHistory CarHistory { get; private set; }

    /// <summary>
    /// Counts the times an attrition rate appears (used only in the beginning of the race)
    /// </summary>
    /// <value>The attrition rate counter.</value>
    public Dictionary<double, int> AttritionRateCounter { get; set; }

    /// <summary>
    /// Gets or sets a value indicating whether attrition rate has been calculated.
    /// </summary>
    /// <value><c>true</c> if attrition rate calculated; otherwise, <c>false</c>.</value>
    public bool AttritionRateCalculated { get; set; }

    #endregion

    #region Constructor

    public RaceData()
    {
        BotName = string.Empty;
        LanesDistanceFromCenter = new List<int>();
        TrackPieces = new List<TrackPiece>();
		TrackSegments = new List<TrackSegment>();
        NextCurvePiece = new List<int>();
        NextCurveDistance = new List<double>();
        LastStraightLineStart = -1;
        NextSlowerPiece = new List<int>();
        NextSlowerPieceDistance = new List<List<double>>();

        GameTick = 0;
        CurrentLap = 0;
        IsCarCrashed = false;
        TurboAvailable = false;
        TurboDuration = 0;
        TurboFactor = 0;
        TurboActive = false;
        CurrentTrackPiece = 0;
        CurrentTrackInPieceDistance = 0;
        CurrentTrackInPieceDistanceGameTick = 0;
        CurrentSpeed = 0;
        MaxSpeed = 0;
        MaxAngleInCurrentTrackPiece = 0;
        MaxSpeedInCurrentTrackPiece = 0;
        NextSwitchIndex = 0;
        NextSwitchEndLane = 0;

        BestDirectionNextSwitch = new Dictionary<int, string>();
        BestDirectionNextSwitchIndex = new Dictionary<int, int>();
        FirstSwitchDirection = null;
        MaxSpeedPerPiecePerLane = new List<List<double>>();
		CarHistory = new CarHistory ();
        OtherCars = new Dictionary<string, CarState>();
        AttritionRateCounter = new Dictionary<double, int>();
        AttritionRateCalculated = false;
    }

    #endregion

    #region Methods

    /// <summary>
    /// Finds the best piece index for turbo, i.e., the longest straight track segment.
    /// </summary>
    /// <returns>The best piece index for turbo</returns>
    private int FindBestPieceIndexForTurbo()
    {
        int FIRST_LANE_INDEX = 0;

        Dictionary<int, double> straightLinesLenght = new Dictionary<int, double>();
        int currentStraightSegmentStartIndex = 0;
        double currentStraightSegmentLength = 0.0;
        for (int i = 0; i < TrackPieces.Count; i++)
        {
            // straight segment, keep counting into current
            if (TrackPieces[i].Angle == 0)
            {
                //length is the same on all lanes, so gets always the first
                currentStraightSegmentLength += TrackPieces[i].Length[FIRST_LANE_INDEX];
            }
            // breaks the previous straight segment, starting a new one on the next index
            else
            {
                straightLinesLenght.Add(currentStraightSegmentStartIndex, currentStraightSegmentLength);
                currentStraightSegmentStartIndex = i + 1;
                currentStraightSegmentLength = 0.0;
            }
        }

        // if there is a straight line segment remaing, sum it up with the beginning of the track
        if (currentStraightSegmentLength > 0)
        {
            currentStraightSegmentLength += straightLinesLenght[0];
            straightLinesLenght.Add(currentStraightSegmentStartIndex, currentStraightSegmentLength);
        }

        // finds the longest straight line segment to use turbo
        int bestPieceIndexForTurbo = 0;
        foreach (KeyValuePair<int, double> straightSegment in straightLinesLenght)
        {
            if (straightSegment.Value > straightLinesLenght[bestPieceIndexForTurbo])
            {
                bestPieceIndexForTurbo = straightSegment.Key;
            }
        }

        return bestPieceIndexForTurbo;
    }

    /// <summary>
    /// Resets the status values.
    /// It needs to be called for every GameInit so the game's status aren't messed up
    /// </summary>
    public void ResetStatusValues()
    {
        GameTick = 0;
        CurrentLap = 0;
        TurboAvailable = false;
        TurboDuration = 0;
        TurboFactor = 0;
        TurboActive = false;
        CurrentTrackPiece = 0;
        CurrentTrackInPieceDistance = 0;
        CurrentTrackInPieceDistanceGameTick = 0;
        CurrentSpeed = 0;
        MaxSpeed = 0;
        MaxAngleInCurrentTrackPiece = 0;
        MaxSpeedInCurrentTrackPiece = 0;

        CarHistory.Clear();
    }

    #endregion
}
