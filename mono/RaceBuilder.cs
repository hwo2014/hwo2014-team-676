using System;

class RaceBuilder: SendMsg
{
	private string raceTypeCommand;
	private string trackName;
	private string botName;
	private string botKey;

	public RaceBuilder(string[] args)
	{
		this.botName = args[2];
		this.botKey = args[3];
		this.trackName = args.Length >= 5 ? args[4] : null;

		if (this.trackName != null) {
			this.raceTypeCommand = "createRace";
		} else {
			this.raceTypeCommand = "join";
		}
	}

	protected override string MsgType() { 
		return raceTypeCommand;
	}

	protected override Object MsgData() {
		Object data = null;

		switch (raceTypeCommand) {
			case "createRace":
				CreateRaceData crData = new CreateRaceData();
				crData.botId.name = this.botName;
				crData.botId.key = this.botKey;
				crData.trackName = this.trackName;
				crData.carCount = 1;
				data = (Object)crData;
				break;
			case "join":
				data = new Join(this.botName, this.botKey);
				break;
		}

		return data;
	}

	public override string ToString() {
		return this.raceTypeCommand + " " + this.trackName;
	}
}

public class BotId {
	public string name;
	public string key;
}

public class CreateRaceData {
	public BotId botId;
	public string trackName;
	public int carCount;

	public CreateRaceData() {
		this.botId = new BotId();
	}
}
