using System;
using Newtonsoft.Json.Linq;

static public class ResponseUtil
{
    /// <summary>
    /// Extracts the index of the my bot.
    /// </summary>
    /// <returns>The index of my bot.</returns>
    /// <param name="raceData">Race data.</param>
    /// <param name="parsedJson">Parsed json.</param>
    public static int ExtractMyBotIndex(RaceData raceData, JArray parsedJson)
    {
        int myBotIndex = -1;
        for (int i = 0; i < parsedJson.Count; i++)
        {
            JToken parsedJsonObject = parsedJson[i];
            if (raceData.BotName.Equals(parsedJsonObject["id"]["name"].ToObject<string>()))
            {
                myBotIndex = i;
                break;
            }
        }
        return myBotIndex;
    }

}

