using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

static class Spawn
{
    /// <summary>
    /// Process the specified msgData and raceData for the Spawn msgType.
    /// </summary>
    /// <param name="msgData">Message data</param>
    /// <param name="raceData">Race data</param>
    internal static SendMsg process(string msgData, RaceData raceData)
    {
        JObject parsedJson = JObject.Parse(msgData);
        if (!raceData.BotName.Equals(parsedJson["name"].ToObject<string>()))
        {
            return Ping.One;
        }

        raceData.IsCarCrashed = false;
        Console.WriteLine(String.Format("Spawned at game tick: {0}.", raceData.GameTick));

        return Ping.One;
    }
}
