using System;
using Newtonsoft.Json.Linq;

static class TurboStart
{
    /// <summary>
    /// Process the specified msgData and raceData for the TurboStart msgType.
    /// </summary>
    /// <param name="msgData">Message data</param>
    /// <param name="raceData">Race data</param>
    internal static SendMsg process(string msgData, RaceData raceData)
    {
        JObject parsedJson = JObject.Parse(msgData);
        if (raceData.BotName.Equals(parsedJson["name"].ToObject<string>()))
        {
            Console.WriteLine("Turbo Start");
            raceData.TurboAvailable = false;
            raceData.TurboActive = true;
        }

        return Ping.One;
    }
}
