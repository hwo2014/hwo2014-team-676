using System;
using System.Collections.Generic;

public class CarHistory
{
    public Dictionary<long, MyCarState> State { get; private set; }

	public CarHistory() {
        State = new Dictionary<long, MyCarState>();
	}

	public void Memorize(long tick, MyCarState state) {
        if (!State.ContainsKey(tick))
        {
            State.Add(tick, state);
        }
	}

	public void Clear() {
        State.Clear();
	}

    public List<MyCarState> GetLastStatesSince(long tick, int numberOfSamples)
    {
        List<MyCarState> values = new List<MyCarState>();
        for (int i = 0; i < numberOfSamples; i++) {
            long currentTick = tick - i;
            if (!State.ContainsKey(currentTick)) {
                numberOfSamples++;
                continue;
            }
            values.Add(State[currentTick]);
        }
        return values;
    }

	private List<KeyValuePair<long, D>> getLastValuesSince<D>(Dictionary<long, D> memory, long tick, int numberOfSamples) {
		List<KeyValuePair<long, D>> values = new List<KeyValuePair<long, D>>();
		for (int i = 0; i < numberOfSamples; i++) {
			long currentTick = tick - i;
			if (!memory.ContainsKey(currentTick)) {
				numberOfSamples++;
				continue;
			}
			values.Add(new KeyValuePair<long, D>(currentTick, memory[currentTick]));
		}
		return values;
	}
}
