using System;

static class GameStart
{
	/// <summary>
	/// Process the specified msgData and raceData for the gameStart msgType.
	/// </summary>
	/// <param name="msgData">Message data</param>
	/// <param name="raceData">Race data</param>
	internal static SendMsg process(string msgData, RaceData raceData)
	{
		Console.WriteLine("Race started");
		raceData.CarHistory.Clear();
        return Ping.One;
	}
}
