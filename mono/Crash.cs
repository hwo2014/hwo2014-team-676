using System;
using System.Collections.Generic;
using Newtonsoft.Json.Linq;

static class Crash
{
	/// <summary>
	/// Process the specified msgData and raceData for the Crash msgType.
	/// </summary>
	/// <param name="msgData">Message data</param>
	/// <param name="raceData">Race data</param>
	internal static SendMsg process(string msgData, RaceData raceData)
	{
        JObject parsedJson = JObject.Parse(msgData);
        if (!raceData.BotName.Equals(parsedJson["name"].ToObject<string>()))
        {
            return Ping.One;
        }

        //persists race data info
        raceData.IsCarCrashed = true;

        // adjust max angle if needed
        if (raceData.CarHistory.State.ContainsKey(raceData.GameTick - 2))
        {
            double currentAngle = Math.Abs(raceData.CurrentAngle);
            double predictedAngle = currentAngle + 
                Math.Abs(currentAngle - raceData.CarHistory.State[raceData.GameTick - 2].CurrentAngle);
            if (predictedAngle < Parameters.MAX_CAR_ANGLE_POSSIBLE)
            {
                Console.WriteLine(string.Format("predicted angle: {0}|\tmax car angle possible: {1}", 
                                            predictedAngle, Parameters.MAX_CAR_ANGLE_POSSIBLE));
                double anglesMultiplier = predictedAngle / Parameters.MAX_CAR_ANGLE_POSSIBLE;
                Parameters.MAX_CAR_ANGLE_POSSIBLE = predictedAngle;
                Parameters.MAX_CAR_ANGLE_BEFORE_CRASHING *= anglesMultiplier;
                Parameters.ANGLE_THRESHOLD_FOR_INCREASING_SPEED_GREATLY *= anglesMultiplier;
                Parameters.ANGLE_THRESHOLD_FOR_INCREASING_SPEED *= anglesMultiplier;
                Parameters.ANGLE_THRESHOLD_FOR_DECREASING_SPEED *= anglesMultiplier;
                Console.WriteLine(string.Format("Adjusted angles parameters multiplying by {0}", anglesMultiplier));
            }
        }

        // Writes crash info to console
        List<MyCarState> carStates = raceData.CarHistory.GetLastStatesSince(raceData.GameTick, 20);
        string angleString = "";
        for (int i = 0; i < carStates.Count; i++)
        {
            angleString += string.Format("{0}: {1} / {2}\n", carStates[i].GameTick, carStates[i].CurrentAngle, carStates[i].Throttle);
        }
        Console.WriteLine(String.Format("Crashed! At game tick: {0}.\n{1}", raceData.GameTick, angleString));

        return Ping.One;
	}
}
